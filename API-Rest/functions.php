<?php
    
    require_once('db.php');

    function Productos(){
        $query = "select * from productos";
    $respuesta = ObtenerRegistros($query);
    return  ConvertirUTF8($respuesta);    
    }

    function ProductosPorID($id){
        $query = "select * from productos where ProductoId = $id";
    $respuesta = ObtenerRegistros($query);
    return  ConvertirUTF8($respuesta);    
    }

    function CrearProducto($array){

        /* deberias validar que existan los campos obligatorios para insertar y para evitar errores al asignarlos a una variable */
        if(!isset($array[0]['Nombre']) || !isset($array[0]['Codigo']) || !isset($array[0]['Presentacion'])){
            
            print_r($array);
            //Error en Json enviado no tiene el formato correcto
            echo "Esta madre retorno false!";
            return false;
        }else{
            echo "Esta madre retorno true!";
            //asignamos los valores a las variables
            $Nombre = $array[0]['Nombre'];
            $Codigo = $array[0]['Codigo'];
            $Presentacion = $array[0]['Presentacion'];
            //todo correcto hasta el momento
                    $query = "INSERT INTO productos (Nombre, Codigo, Presentacion) VALUES ('$Nombre', '$Codigo', '$Presentacion')";
                    /*La funcion NonQuery retorna la cantidad de filas afectadas (en este caso retornara un 1 si guardo y un 0 si no guardo)
                        en caso que retorne 0  deberas  descomentar la siguiente linea  para ver el query que se envia a MySQL ,
                        lo copias y lo ejecutas en la base de datos y confirmas que no te muestre error*/
                    
                    //print_r($query);

                    $verificar = NonQuery($query);
                    if($verificar >= 1){
                        return true;
                    }else{
                        return false;
                    } 
        } 
    }
?>