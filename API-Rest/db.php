<?php
    
    $server = "localhost";
    $user = "root";
    $db = "db_almacenes";
    $pass = "";
    $port = "3306";

    //Crear conexion
    $conexion = new mysqli($server, $user, $pass, $db, $port);
    if($conexion -> connect_errno){
        die($conexion -> connect_error);
    }
    
    //guardar, modificar, eliminar
    function NonQuery($sqlstr, &$conexion = null){
        if(!$conexion)global $conexion; // si el usuario lo deja null el parametro se toma de la variable creada anteriormente
        return $conexion -> affected_rows;
    }
    //select
    function ObtenerRegistros($sqlstr, &$conexion = null){
        if(!$conexion)global $conexion; // si el usuario lo deja null el parametro se toma de la variable creada anteriormente
        $result = $conexion -> query($sqlstr);
        $resultArray = array();
        foreach($result as $registros){
            $resultArray[] = $registros;
        }
        return $resultArray;
    }
    //utf8
    function ConvertirUTF8 ($array){

        array_walk_recursive($array, function(&$item,$key){
            if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
            }
        });
        return $array;

    }
?>